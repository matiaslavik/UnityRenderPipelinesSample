using UnityEngine;
using UnityEngine.Rendering.Universal;

public class BlurFeature : ScriptableRendererFeature
{
    [System.Serializable]
    public class PassSettings
    {
        public RenderPassEvent renderPassEvent = RenderPassEvent.AfterRenderingTransparents;
        [Range(0, 20)]
        public int blurStrength = 5;
    }

    BlurPass pass;
    public PassSettings passSettings = new PassSettings();

    public override void Create()
    {
        pass = new BlurPass(passSettings);
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        renderer.EnqueuePass(pass);
    }
}