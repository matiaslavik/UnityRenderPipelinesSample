using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

/// <summary>
/// Custom pass that blurs everything that is occluded by an object marked with "Glass" layer.
/// Ths could be used for rendering foggy glass.
/// The pass consists of two steps:
///  1. Render all glass geometry to a texture.
///  2. Blur everything that is occluded by the glass geometry.
/// </summary>
public class BlurPass : ScriptableRenderPass
{
    private BlurFeature.PassSettings passSettings;

    private int prePassBufferID = Shader.PropertyToID("_PrePassBuffer");
    private int temporaryBufferID = Shader.PropertyToID("_TemporaryBuffer");

    private RenderTargetIdentifier prePassBuffer;
    private RenderTargetIdentifier temporaryBuffer;
    private LayerMask glassLayer = ~0;

    private Material prePassMaterial;
    private Material fullscreenBlurMaterial;
    private static readonly ShaderTagId shaderTag = new ShaderTagId("UniversalForward");

    private RenderStateBlock renderStateBlock;
    private int prePassTextureId = Shader.PropertyToID("_BlurPrePassTexture");

    public BlurPass(BlurFeature.PassSettings passSettings)
    {
        this.passSettings = passSettings;
        renderPassEvent = passSettings.renderPassEvent;

        glassLayer = LayerMask.GetMask("Glass");

        renderStateBlock = new RenderStateBlock(RenderStateMask.Nothing);
    }

    public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData)
    {
        // Create render targets
        RenderTextureDescriptor descriptor = renderingData.cameraData.cameraTargetDescriptor;
        descriptor.depthBufferBits = 0;
        cmd.GetTemporaryRT(temporaryBufferID, descriptor, FilterMode.Bilinear);
        temporaryBuffer = new RenderTargetIdentifier(temporaryBufferID);
        cmd.GetTemporaryRT(prePassBufferID, descriptor, FilterMode.Bilinear);
        prePassBuffer = new RenderTargetIdentifier(prePassBufferID);

        // Create blur pre-pass material (for rendering glass geometry to texture)
        prePassMaterial = CoreUtils.CreateEngineMaterial("Custom/BlurPrePass");
        prePassMaterial.SetColor("_Colour", Color.red);

        // Create fullscreen blur material (for blurring everything behind glass)
        fullscreenBlurMaterial = CoreUtils.CreateEngineMaterial("Custom/BlurPass");
        Debug.Assert(fullscreenBlurMaterial != null, "Failed to create fullscreen pass material");
        fullscreenBlurMaterial.SetInt("_BlurStrength", passSettings.blurStrength);
    }

    public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
    {
        CommandBuffer cmd = CommandBufferPool.Get();
        RenderTargetIdentifier colorBuffer = renderingData.cameraData.renderer.cameraColorTarget;
        RenderTargetIdentifier depthBuffer = renderingData.cameraData.renderer.cameraDepthTarget;
        
        using (new ProfilingScope(cmd, new ProfilingSampler("Blur pre-pass")))
        {
            // Set render target: Render to pre-pass buffer (note: we use the camera's existing depth buffer for Z test)
            CoreUtils.SetRenderTarget(cmd, prePassBuffer, depthBuffer, ClearFlag.Color);
            context.ExecuteCommandBuffer(cmd);
            cmd.Clear();
            
            // Render all geometry marked with "Glass" layer. These will blur what's behind them.
            DrawingSettings drawingSettings = CreateDrawingSettings(shaderTag, ref renderingData, SortingCriteria.CommonTransparent);
            drawingSettings.overrideMaterial = prePassMaterial;
            RenderQueueRange renderQueueRange = RenderQueueRange.transparent;
            FilteringSettings filteringSettings = new FilteringSettings(renderQueueRange, glassLayer);
            context.DrawRenderers(renderingData.cullResults, ref drawingSettings, ref filteringSettings, ref renderStateBlock);
        
        }
        
        // Use render target from above as input texture for our blur shader.
        cmd.SetGlobalTexture(prePassTextureId, prePassBuffer);
        context.ExecuteCommandBuffer(cmd);
        cmd.Clear();
        using (new ProfilingScope(cmd, new ProfilingSampler("Fullscreen blur pass")))
        {
            // Copy to Camera's colour buffer to temporary buffer
            Blit(cmd, colorBuffer, temporaryBuffer);
            // Blit to Camera's colour buffer, using our blur shader.
            Blit(cmd, temporaryBuffer, colorBuffer, fullscreenBlurMaterial);
        }

        context.ExecuteCommandBuffer(cmd);
        CommandBufferPool.Release(cmd);
    }

    public override void OnCameraCleanup(CommandBuffer cmd)
    {
        cmd.ReleaseTemporaryRT(prePassBufferID);
        cmd.ReleaseTemporaryRT(temporaryBufferID);
    }
}
