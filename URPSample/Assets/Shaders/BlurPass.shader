Shader "Custom/BlurPass"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white"
    }

    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline"
        }

        HLSLINCLUDE
        #pragma vertex vert
        #pragma fragment frag

        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

        struct Attributes
        {
            float4 positionOS : POSITION;
            float2 uv : TEXCOORD0;
        };

        struct Varyings
        {
            float4 positionHCS : SV_POSITION;
            float2 uv : TEXCOORD0;
        };

        TEXTURE2D(_MainTex);

        SAMPLER(sampler_MainTex);
        float4 _MainTex_TexelSize;
        float4 _MainTex_ST;

        TEXTURE2D(_BlurPrePassTexture);

        SAMPLER(sampler_BlurPrePassTexture);
        float4 _BlurPrePassTexture_TexelSize;
        float4 _BlurPrePassTexture_ST;

        int _BlurStrength;

        Varyings vert(Attributes IN)
        {
            Varyings OUT;
            OUT.positionHCS = TransformObjectToHClip(IN.positionOS.xyz);
            OUT.uv = TRANSFORM_TEX(IN.uv, _MainTex);
            return OUT;
        }
        ENDHLSL

        Pass
        {
            Name "Blur"

            HLSLPROGRAM
            half4 frag(Varyings IN) : SV_TARGET
            {
                float2 resolution = _MainTex_TexelSize.xy;

                int samples = 2 * _BlurStrength + 1;

                half4 sum = 0;
                for (float y = 0; y < samples; y++)
                    sum += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uv + float2(0, y - _BlurStrength) * resolution);
                for (float x = 0; x < samples; x++)
                    sum += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uv + float2(x - _BlurStrength, 0) * resolution);

                int blurFactor = SAMPLE_TEXTURE2D(_BlurPrePassTexture, sampler_BlurPrePassTexture, IN.uv).r;
                if (blurFactor > 0.0)
                    return sum / (samples * 2);
                else
                    return SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uv);
            }
            ENDHLSL
        }
    }
}
