using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering;

/// <summary>
/// Custom pass that blurs everything that is occluded by an object marked with "Glass" layer.
/// Ths could be used for rendering foggy glass.
/// The pass consists of two steps:
///  1. Render all glass geometry to a texture.
///  2. Blur everything that is occluded by the glass geometry.
/// </summary>
public class BlurPass : CustomPass
{
    [Range(0, 20)]
    public int blurStrength = 10;

    private Shader fullscreenShader;
    private Material fullscreenMaterial;
    private Shader prePassShader;
    private Material prePassMaterial;
    private RTHandle intermediateRenderTarget;
    private LayerMask glassLayer = ~0;

    protected override void Setup(ScriptableRenderContext renderContext, CommandBuffer cmd)
    {
        // Create pre-pass shader/material
        prePassShader = Shader.Find("Custom/BlurPrePass");
        prePassMaterial = CoreUtils.CreateEngineMaterial(prePassShader);
        Debug.Assert(prePassMaterial != null, "Failed to create pre-pass material");

        // Create fullscreen shader/material
        fullscreenShader = Shader.Find("Custom/BlurPass");
        fullscreenMaterial = CoreUtils.CreateEngineMaterial(fullscreenShader);
        Debug.Assert(fullscreenMaterial != null, "Failed to create fullscreen pass material");

        glassLayer = LayerMask.GetMask("Glass");

        // Create render target for pre-pass
        intermediateRenderTarget = RTHandles.Alloc(
            Vector2.one, TextureXR.slices, dimension: TextureXR.dimension,
            colorFormat: GraphicsFormat.B10G11R11_UFloatPack32,
            useDynamicScale: true, name: "Immediate Test Buffer"
        );
    }

    protected override void Execute(CustomPassContext ctx)
    {
        fullscreenMaterial.SetInt("_BlurStrength", blurStrength);

        // Pre pass
        CoreUtils.SetRenderTarget(ctx.cmd, intermediateRenderTarget, ClearFlag.Color);
        CustomPassUtils.DrawRenderers(ctx, glassLayer, RenderQueueType.All, prePassMaterial);
        
        // Fullscreen pass
        ctx.propertyBlock.SetTexture("_BlurPrePassTexture", intermediateRenderTarget);
        CoreUtils.SetRenderTarget(ctx.cmd, ctx.cameraColorBuffer, ClearFlag.None);
        CoreUtils.DrawFullScreen(ctx.cmd, fullscreenMaterial, ctx.propertyBlock, shaderPassId: 0);
    }

    protected override void Cleanup()
    {
        CoreUtils.Destroy(prePassMaterial);
        CoreUtils.Destroy(fullscreenMaterial);
        intermediateRenderTarget.Release();
    }
}