# UnityRenderPipelinesSample

Sample project showing how to render a class that blurs what's behind it, using both URP and HDRP.

![](Screenshot.jpg)

## Implementation

The blur effect is implemented as a custom render pass (URP: `ScriptableRenderPass`, HDRP: `CustomPass`), and is done in two steps:
1. Pre-pass: Render all geometry marked as "Glass" layer to a texture. 
2. Blur pass (fullscreen blit): Blur all pixels behind the glass (lookup texture from previous step to decide if it's behind the glass).
